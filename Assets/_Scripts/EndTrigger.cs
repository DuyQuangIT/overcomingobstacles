using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndTrigger : MonoBehaviour
{
    public GameObject levelComplete;
    public float timePlay = 0.4f;

    public AudioSource audioSource;
    public AudioClip audioWin;

    [Header("Turn off MoveLeft and MoveRight UI")]
    public GameObject MoveLeft;
    public GameObject MoveRight;

    void OnTriggerEnter(Collider other)
    {
        Invoke("setLevelComplete", timePlay);

        audioSource.clip = audioWin;
        audioSource.Play();
    }

    void setLevelComplete()
    {
        levelComplete.SetActive(true);

        MoveLeft.SetActive(false);
        MoveRight.SetActive(false);
    }
}
