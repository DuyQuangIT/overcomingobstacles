﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallTrigger : MonoBehaviour
{
    public Rigidbody rb;
    public GameManager gameManager;
    public float gravity = -2f;
    public GameObject limit;

    public AudioSource audioSource;
    public AudioClip audioLose;

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Wall")
        {
            rb.useGravity = true;

            limit.SetActive(true);

            audioSource.clip = audioLose;
            audioSource.Play();
        }
    }

    void Update()
    {
        if (transform.position.y <= gravity)
        {
            gameManager.EndGame();
        }
    }
}
