﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MobileMoveLeft : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public GameObject player; // Tham chiếu đến GameObject player
    public float moveSpeed = 5f; // Tốc độ di chuyển của player

    private bool isHoldingButton = false; // Biến xác định xem nút button có đang được nhấn giữ hay không

    private void FixedUpdate()
    {
        if (isHoldingButton)
        {
            // Di chuyển player sang bên trái
            player.transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        // Khi nút button được nhấn giữ
        isHoldingButton = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        // Khi nút button được nhả ra
        isHoldingButton = false;
    }
}
