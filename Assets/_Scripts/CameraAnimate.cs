using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraAnimate : MonoBehaviour
{
    public float timeNextLevel = 5.5f;

    // Update is called once per frame
    void Update()
    {
        Invoke("nextLevel", timeNextLevel);
    }

    void nextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
