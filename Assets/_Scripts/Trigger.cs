﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
    public GameObject[] obstacle;
    public GameObject explosion;
    public GameObject boom;
    public GameObject lose;

    public AudioSource audioSource;
    public AudioClip audioBoom;

    [Header("UI mobile")]
    public MobileMoveLeft mobileLeft;
    public MobileMoveRight mobileRight;

    void Start()
    {
        //audioSource = gameObject.GetComponent<AudioSource>();
        //audioSource.loop = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Obstacle")
        {
            Destroy(gameObject); // xóa Player
            // Destroy(other.gameObject) - xóa enemy

            FindObjectOfType<FollowPlayer>().enabled = false;

            Power();

            FindObjectOfType<Score>().enabled = false;

            lose.SetActive(true);

            audioSource.clip = audioBoom;
            audioSource.Play();

            mobileLeft.enabled = false;
            mobileRight.enabled = false;
        }
    }

    void Power()
    {
        Instantiate(explosion, transform.position, transform.rotation);
        Instantiate(boom, transform.position, transform.rotation);
    }
}
