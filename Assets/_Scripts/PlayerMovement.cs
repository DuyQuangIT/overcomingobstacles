﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float forwardPlayer = 40f;
    public float sidePlayer = 40f;

    public float nextTime = 5f; // 1 khoảng thời gian tiếp theo
    public float oldTime; // thời gian cũ đã chạy rồi
    public float moreTime = 5f; // thời gian chạy thêm

    public float maxY = 4f;
    public float speedUp = 1f;

    // Update is called once per frame
    public void FixedUpdate() // cố định khung hình trên giây, giúp player chạy mượt hơn
    {
        // Thời gian Player biến hình và bay lên
        // Nếu thời gian hiện tại = t/gian cũ + t/gian mới
        if (Time.time > oldTime + nextTime)
        {
            // nếu vị trí y <= maxY thì thực hiện bay lên
            if (transform.position.y <= maxY)
            {
                transform.Translate(Vector3.up * speedUp * Time.deltaTime);
            }
        }

        // Thời gian này là thời gian cần chờ Player biến hình và bay lên hoàn thành rồi mới di chuyển
        // Nếu thời gian hiện tại = t/gian cũ + t/gian mới(khoảng t/gian này đã xét ở trên) + t/gian chạy thêm
        if (Time.time > oldTime + nextTime + moreTime)
        {
            transform.Translate(Vector3.forward * forwardPlayer * Time.deltaTime);

            keyMove();
        }

        //transform.Translate(Vector3.forward * forwardPlayer * Time.deltaTime);

        //keyMove();
    }

    void keyMove()
    {
        if(Input.GetKey("a"))
        {
            transform.Translate(Vector3.left * sidePlayer * Time.deltaTime);
        }

        if (Input.GetKey("d"))
        {
            transform.Translate(Vector3.right * sidePlayer * Time.deltaTime); // deltaTime là thời gian giữa hai khung hình (frame) liên tiếp của game
        }
    }
   
}
