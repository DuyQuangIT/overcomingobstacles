﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    bool gameHasEnded = false; // phụ thuộc vào thuộc tính mà hàm của Script khác gọi
    public float restartDelay = 1f;

    public void EndGame()
    {
        if (gameHasEnded == false)
        {
            gameHasEnded = true;
            Invoke("Restart", restartDelay); // chạy 1 hàm với 1 khoảng thời gian nhất định
        }
    }

    public void Restart()
    {
        // load scene với tên của cảnh hiện tại 
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
