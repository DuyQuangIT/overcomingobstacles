﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyTrigger : MonoBehaviour
{
    public GameManager gameManager;

    void OnTriggerEnter(Collider other)
    {
        // Nếu va chạm với vật thể có tag = "Obstacle" thì chạy hàm EndGame() của GameManager
        if (other.gameObject.tag == "Obstacle")
        {
            gameManager.EndGame();
        }
    }
}
