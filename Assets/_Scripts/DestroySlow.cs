using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySlow : MonoBehaviour
{
    public float slowForwardPlayer = 20f;

    public AudioSource audioSource;
    public AudioClip audioSlow;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Slow")
        {
            Destroy(other.gameObject); // x�a Player
            FindObjectOfType<PlayerMovement>().forwardPlayer = slowForwardPlayer;

            audioSource.clip = audioSlow;
            audioSource.Play();
        }
    }
}
