using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyFlash : MonoBehaviour
{
    public float flashForwardPlayer = 60f;

    public AudioSource audioSource;
    public AudioClip audioFlash;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Flash")
        {
            Destroy(other.gameObject); // x�a Player
            FindObjectOfType<PlayerMovement>().forwardPlayer = flashForwardPlayer;

            audioSource.clip = audioFlash;
            audioSource.Play();
        }
    }
}
